<?php


namespace Its\DataStore\Controller\Index;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Result\PageFactory;

use Its\DataStore\Api\DataStoreRepositoryInterface;
use Its\DataStore\Api\Data\DataStoreInterface;
class Index extends Action
{
    protected $_pageFactory;
    protected $_datastoreRepository;
    protected $_datastoreModel;

    public function __construct(
        Context $context,
        PageFactory $pageFactory,
        DataStoreRepositoryInterface $datastoreRepository,
        DataStoreInterface $datastoreInterface
    ) {
        $this->_pageFactory = $pageFactory;
        $this->_datastoreRepository= $datastoreRepository;
        $this->_datastoreModel = $datastoreInterface;
        return parent::__construct($context);
    }

    public function execute()
    {
        echo "Hello World!!!";
        //Create a record.
        $this->_datastoreModel->setTitle("Title");
        $this->_datastoreModel->setContent("Content");

        try {
            $this->_datastoreRepository->save($this->_datastoreModel);
        } catch (CouldNotSaveException $e) {
            echo $e->getMessage();
        }
        //Read a record
        try {
            $datastore = $this->_datastoreRepository->getById("19");
            echo "DataStore id = " . $datastore->getId() . "<br>";
            echo "DataStore Title = " . $datastore->getTitle();
        } catch (NoSuchEntityException $e) {
            echo "No such entity exception - " . $e->getMessage();
        } catch (LocalizedException $e) {
            echo "Localized Exception" . $e->getMessage();
        }
        //Update a record
        try {
            $datastore = $this->_datastoreRepository->getById("21");
            echo "DataStore id = " . $datastore->getId() . "<br>";
            echo "DataStore Title = " . $datastore->getTitle();
            $datastore->setTitle("This is the updated title");
            $datastore->setContent("This is the updated Content");
            $this->_datastoreRepository->save($datastore);
        } catch (NoSuchEntityException $e) {
            echo "No such entity exception - " . $e->getMessage();
        } catch (LocalizedException $e) {
            echo "Localized Exception" . $e->getMessage();
        }
        //Delete a record
        try {
            $this->_datastoreRepository->deleteById("20");
            echo "Deleted the record with id = 20" . "<br>" . "Go to database and check.";
        } catch (NoSuchEntityException $e) {
            echo "No such entity exception - " . $e->getMessage();
        } catch (LocalizedException $e) {
            echo "Localized Exception" . $e->getMessage();
        }
        exit;
    }


}
