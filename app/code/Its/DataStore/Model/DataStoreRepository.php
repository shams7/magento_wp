<?php


namespace Its\DataStore\Model;


use Its\DataStore\Api\Data\DataStoreInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResultsInterfaceFactory;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Its\DataStore\Api\DataStoreRepositoryInterface;
use Its\DataStore\Model\DataStoreFactory;
use Its\DataStore\Model\ResourceModel\DataStore as ObjectResourceModel;
use Its\DataStore\Model\ResourceModel\DataStore\CollectionFactory;
use Magento\Cms\Model\BlockRepository;
use Magento\Cms\Model\BlockFactory;
use Magento\Framework\Api\FilterBuilder;
use Magento\Framework\Api\SearchCriteriaBuilder;
use \Zend_Db_Expr;
use function Safe\eio_chmod;

/**
 * Class DataStoreRepository
 */

class DataStoreRepository implements DataStoreRepositoryInterface

{
    //protected $blockFactory;

    protected $blockFactory;
    protected $blockRepository;
    protected $objectFactory;
    protected $objectResourceModel;
    protected $collectionFactory;
    protected $searchResultsFactory;
    protected $filterBuilder;
    protected $searchCriteriaBuilder;
    protected $_datastoreModel;
    /**
     * DataStoreRepository constructor.
     *
     * @param DataStoreFactory $objectFactory
     * @param ObjectResourceModel $objectResourceModel
     * @param CollectionFactory $collectionFactory
     * @param SearchResultsInterfaceFactory $searchResultsFactory
     */
    public function __construct(
        DataStoreFactory $objectFactory,
        ObjectResourceModel $objectResourceModel,
        CollectionFactory $collectionFactory,
        SearchResultsInterfaceFactory $searchResultsFactory,
        BlockFactory $blockFactory,
        BlockRepository $blockRepository,
        FilterBuilder  $filterBuilder,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        DataStoreInterface $dataStoreinterface
    ) {
        $this->objectFactory        = $objectFactory;
        $this->objectResourceModel  = $objectResourceModel;
        $this->collectionFactory    = $collectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->blockFactory = $blockFactory;
        $this->blockRepository = $blockRepository;
        $this->filterBuilder = $filterBuilder;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->_datastoreModel = $dataStoreinterface;
    }
    /**
     * @inheritDoc
     *
     * @throws CouldNotSaveException
     */
    public function save(DataStoreInterface $object)
    {
        try {
            $this->objectResourceModel->save($object);
        } catch (\Exception $e) {
            throw new CouldNotSaveException(__($e->getMessage()));
        }
        return $object;
    }

    /**
     * @inheritDoc
     */
    public function getById($id)
    {
        $object = $this->objectFactory->create();
        $this->objectResourceModel->load($object, $id);
        if (!$object->getId()) {
            throw new NoSuchEntityException(__('Object with id "%1" does not exist.', $id));
        }

        return $object;
    }
    /**
     * @inheritDoc
     */
    public function getByItemId($itemid)
    {
        $object = $this->objectFactory->create();
        $this->objectResourceModel->load($object, $itemid);
        if (!$object->getItemId()) {
            throw new NoSuchEntityException(__('Object with itemid "%1" does not exist.', $itemid));
        }
        return $object;
    }

    public function getByWpId ($itemid)
    {
        $filter = $this->filterBuilder
            ->setField('item_id')
            ->setConditionType('==')
            ->setValue($itemid)
            ->create();
        $this->searchCriteriaBuilder->addFilters([$filter]);
        $searchCriteria = $this->searchCriteriaBuilder->create();
        //$this->_datastoreModel = current($this->getList($searchCriteria)->getItems());
        $pagelist = $this->getList($searchCriteria)->getItems();
        foreach ($pagelist as $obj)
        {
            $this->_datastoreModel = $obj;
           // var_dump($this->_datastoreModel->getTitle());
        }
        //var_dump($this->_datastoreModel->getTitle());

         return   $this->_datastoreModel;

    }


    /**
     * @inheritDoc
     */
    public function getList(SearchCriteriaInterface $criteria)
    {
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        $collection = $this->collectionFactory->create();
        foreach ($criteria->getFilterGroups() as $filterGroup) {
            $fields = [];
            $conditions = [];
            foreach ($filterGroup->getFilters() as $filter) {
                $condition = $filter->getConditionType() ? $filter->getConditionType() : 'eq';
                $fields[] = $filter->getField();
                $conditions[] = [$condition => $filter->getValue()];
            }
            if ($fields) {
                $collection->addFieldToFilter($fields, $conditions);
            }
        }
        $searchResults->setTotalCount($collection->getSize());
        $sortOrders = $criteria->getSortOrders();
        if ($sortOrders) {
            /** @var SortOrder $sortOrder */
            foreach ($sortOrders as $sortOrder) {
                $collection->addOrder(
                    $sortOrder->getField(),
                    ($sortOrder->getDirection() == SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        }
        $collection->setCurPage($criteria->getCurrentPage());
        $collection->setPageSize($criteria->getPageSize());
        $objects = [];
        foreach ($collection as $objectModel) {
            $objects[] = $objectModel;
        }
        $searchResults->setItems($objects);
        return $searchResults;

    }

    /**
     * @inheritDoc
     */
    public function delete(DataStoreInterface $object)
    {
        try {
            $this->objectResourceModel->delete($object);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }
        return true;
    }

    /**
     * @inheritDoc
     */
    public function deleteById($id)
    {
        return $this->delete($this->getById($id));
    }
    /**
     * @inheritDoc
     */
    public function getListData()
    {
        $object = $this->objectFactory->create();
        $collection = $object->getCollection();
        //return $collection->getData();
        return $collection;
    }
    public function getListDataStore()
    {
        $object = $this->objectFactory->create();
        $collection = $object->getCollection();
        return $collection->getData();
        //return $collection;
    }
    /**
     * @inheritDoc
     */
    public function insertBlockCms()
    {
        $data = [
            'title' => 'aa',
            'identifier' => 'aa-aa',
            'stores' => ['0'],
            'is_active' => 1,
            'content' => 'aa'
        ];
        $newBlock = $this->blockFactory->create(['data' => $data]);
        $this->blockRepository->save($newBlock);

    }

    public function getMaxDate()
    {
        $object = $this->objectFactory->create();
        $collection = $object->getCollection()
            ->addFieldToSelect(new Zend_Db_Expr('MAX(updated_date) AS updated_date'))
            ->getFirstItem();


        $maxdate = $collection->getData('updated_date');
        return $maxdate;
    }

}
