<?php


namespace Its\DataStore\Model;


use Its\DataStore\Api\Data\DataStoreInterface;
use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractModel;
use Its\DataStore\Model\ResourceModel\DataStore as ResourceModel;

/**
 * Class DataStore
 */
class DataStore extends AbstractModel implements
    DataStoreInterface,
    IdentityInterface
{
    const CACHE_TAG = 'cms_content';

    /**
     * Init
     */
    protected function _construct() // phpcs:ignore PSR2.Methods.MethodDeclaration
    {
        $this->_init(ResourceModel::class);
    }

    /**
     * @inheritDoc
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }
    public function getId()
    {
        return $this->getData('entity_id');
    }

    /**
     * @inheritDoc
     */
    public function setId($entity_id)
    {
        return $this->setData('entity_id', $entity_id);
    }

    /**
     * @inheritDoc
     */
    public function getTitle()
    {
        return $this->getData('title');
    }

    /**
     * @inheritDoc
     */
    public function setTitle($title)
    {
        return $this->setData('title', $title);
    }

    /**
     * @inheritDoc
     */
    public function getContent()
    {
        return $this->getData('content');
    }

    /**
     * @inheritDoc
     */
    public function setContent($content)
    {
        return $this->setData('content', $content);

    }

    /**
     * @inheritDoc
     */
    public function getItemId()
    {
        return $this->getData('item_id');
    }

    /**
     * @inheritDoc
     */
    public function setItemId($item_id)
    {
        return $this->setData('item_id', $item_id);
    }

    /**
     * @inheritDoc
     */
    public function getCss()
    {
        return $this->getData('css');
    }

    /**
     * @inheritDoc
     */
    public function setCss($css)
    {
        return $this->setData('css', $css);
    }


    public function getUpdatedDate()
    {
        return $this->getData('updated_date');
    }

    /**
     * @inheritDoc
     */
    public function setUpdatedDate($updated_date)
    {
        return $this->setData('updated_date', $updated_date);
    }
}
