<?php


namespace Its\DataStore\Model\ResourceModel;

/**
 * Class DataStore
 */
class DataStore extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * Init
     */
    protected function _construct() // phpcs:ignore PSR2.Methods.MethodDeclaration
    {
        $this->_init('cms_content', 'entity_id');
    }
}
