<?php


namespace Its\DataStore\Model\ResourceModel\DataStore;
use Its\DataStore\Model\DataStore as Model;
use Its\DataStore\Model\ResourceModel\DataStore as ResourceModel;


class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * Init
     */
    protected function _construct() // phpcs:ignore PSR2.Methods.MethodDeclaration
    {
        $this->_init(Model::class, ResourceModel::class);
    }


}
