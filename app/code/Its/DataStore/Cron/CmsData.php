<?php


namespace Its\DataStore\Cron;

use Its\DataStore\Api\Data\DataStoreInterface;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\HTTP\Client\Curl;
use Its\DataStore\Model\DataStore;
use Its\DataStore\Api\DataStoreRepositoryInterface;



class CmsData
{
    /**
     * @var \Magento\Framework\HTTP\Client\Curl
     */
    protected $_curl;
    protected $_datastoreRepository;
    protected $_datastoreModel;
    protected $helperData;
    protected $_logger;
    protected $_httpClientFactory;
    private $maxdate;



   public function __construct(
       Curl $curl,
       Context $context,
       DataStoreRepositoryInterface $datastoreRepository,
       DataStoreInterface $dataStoreinterface,
       \Its\DataStore\Helper\Data $helperData,
       \Psr\Log\LoggerInterface $logger,
       \Magento\Framework\HTTP\ZendClientFactory $httpClientFactory


   ) {
       $this->_curl = $curl;
       $this->_datastoreRepository= $datastoreRepository;
       $this->_datastoreModel = $dataStoreinterface;
       $this->helperData = $helperData;
       $this->_logger = $logger;
       $this->_httpClientFactory = $httpClientFactory;



       //parent::__construct($context);

    }

    public function getDataFromApi()
    {
        //http://wp.test/wp-json/wp/v2/pages?after=2021-02-15%2012:19:38&date_query_column=post_modified
        $this->maxdate = $this->_datastoreRepository->getMaxDate();
        $this->_logger->info("9696969696");
        $this->_logger->info($this->maxdate);
        $params = [
            'after' => $this->maxdate,
            'date_query_column' =>'post_modified',

        ];
        $output = [];
        $url = $this->helperData->getGeneralConfig('url'). '?' . http_build_query($params);
        $this->_logger->info("ööööööööööööööööööööööööööö");
        $this->_logger->info($url);
        //$url = 'http://wp.test/wp-json/wp/v2/pages?after=2021-02-15%2012:19:39&date_query_column=post_modified';
        $username = $this->helperData->getGeneralConfig('user');
        $password = $this->helperData->getGeneralConfig('pass');
        if(!empty($username) || !empty($password))
        {
            $this->_curl->setCredentials($username, $password);
            $this->_curl->get($url);
            $this->_logger->info('-*-*-*-*-*-*-*-');
            $this->_logger->info(var_export($params,true));


            $this->_logger->info($this->_curl->getStatus()."33333333333333333333333333333333");
            if($this->_curl->getStatus() == 200){
                $data = $this->_curl->getBody();
                $this->_logger->info("updatedupdatedupdatedupdated");
                $data = json_decode($data);

        $output = $data;

        }else {
            $this->_logger->info("user or password ara invalid");

        }
        }
        /*foreach ($output as $o)
        {
            $this->_logger->info("blobloblo");
            $this->_logger->info(var_export($o,true));
        }*/
        return $output;

    }



   public function getData()
   {
       $this->getDataFromDirectus();
           //$this->getDataFromApi();
          //$this->storeDataFrmApi();
      /* $this->_logger->info("zzzzzzzz");
       $this->_logger->info($this->maxdate);
       $this->_logger->info("zzzzzzzzz");
       $this->_datastoreModel->setTitle('title');
       $this->_datastoreModel->setContent('blabla');
       $this->_datastoreModel->setUpdatedDate('2021-02-15 12:18:26');
       $this->_datastoreModel->setItemId(232);
       $this->_datastoreModel->setCss('CssExample');
       $this->_datastoreRepository->save($this->_datastoreModel);*/
   }
   public function storeDataFrmApi()
   {
       $result = $this->getDataFromApi();
       foreach ($result as $c) {
           $this->_logger->info("454545");
           $this->_datastoreModel = $this->_datastoreRepository->getByWpId($c->id);
           $this->_logger->info("m3ebi");
           $this->_datastoreModel->setTitle($c->title->rendered);
           $this->_datastoreModel->setContent($c->content->rendered);
           $this->_datastoreModel->setUpdatedDate($c->modified);
           $this->_datastoreModel->setItemId($c->id);
            $this->_datastoreModel->setCss('CssExample');


           try {
               $this->_datastoreRepository->save($this->_datastoreModel);
               $this->_datastoreModel->unsetData();
           } catch (CouldNotSaveException $e) {
               echo $e->getMessage();
           }



       }

   }

   public function getDataFromDirectus()
   {
       $output = [];
       $url = 'http://directus.test/directus/items/pages?fields=*.*.*.*.*.*.*.*.*.*.*.*';
       $this->_curl->get($url);
       $data = $this->_curl->getBody();
       $this->_logger->info("rowsrowsrowsrows");
       $data = json_decode($data);
       $output = $data->data;
       foreach ($output as $e):
           $this->_logger->info(var_export($e->rows,true));
       endforeach;
       return $output;

   }
}

