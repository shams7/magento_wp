<?php


namespace Its\DataStore\Block\Adminhtml\Block\Widget;


use Its\DataStore\Api\DataStoreRepositoryInterface;
use Its\DataStore\Cron\CmsData;

/**
 * CMS block chooser for Wysiwyg CMS widget
 */
class Chooser extends \Magento\Backend\Block\Widget\Grid\Extended
{
    /**
     * @var \Its\DataStore\Model\DataStore
     */
    protected $_datastoreFactory;
    protected $_datastoreRepository;
    protected $_cmsdata;

    /**
     * @var \Magento\Cms\Model\ResourceModel\Block\CollectionFactory
     */
    protected $_collectionFactory;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param \Its\DataStore\Model\DataStoreFactory $datastoreFactory
     * @param \Magento\Cms\Model\ResourceModel\Block\CollectionFactory $collectionFactory
     * @param array $data
     */
    public function __construct(
        DataStoreRepositoryInterface $datastoreRepository,
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Its\DataStore\Model\DataStoreFactory $datastoreFactory,
        \Magento\Cms\Model\ResourceModel\Block\CollectionFactory $collectionFactory,
        \Its\DataStore\Cron\CmsData $cmsData,
        array $data = []
    ) {
        $this->_datastoreRepository= $datastoreRepository;
        $this->_datastoreFactory = $datastoreFactory;
        $this->_collectionFactory = $collectionFactory;
        parent::__construct($context, $backendHelper, $data);
        $this->_cmsdata = $cmsData;
    }

    /**
     * Block construction, prepare grid params
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setDefaultSort('block_identifier');
        $this->setDefaultDir('ASC');
        $this->setUseAjax(true);
        $this->setDefaultFilter(['chooser_is_active' => '1']);
    }

    /**
     * Prepare chooser element HTML
     *
     * @param \Magento\Framework\Data\Form\Element\AbstractElement $element Form Element
     * @return \Magento\Framework\Data\Form\Element\AbstractElement
     */
    public function prepareElementHtml(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        $uniqId = $this->mathRandom->getUniqueHash($element->getId());
        $sourceUrl = $this->getUrl('datastore/block_widget/chooser', ['uniq_id' => $uniqId]);
        $test  = $this->_datastoreRepository->getListData();

        $chooser = $this->getLayout()->createBlock(
            \Magento\Widget\Block\Adminhtml\Widget\Chooser::class
        )->setElement(
            $element
        )->setConfig(
            $this->getConfig()
        )->setFieldsetId(
            $this->getFieldsetId()
        )->setSourceUrl(
            $sourceUrl
        )->setUniqId(
            $uniqId
        );

        if ($element->getValue()) {

            $data = $this->_datastoreFactory->create()->load($element->getValue());
            if ($data->getId()) {
                $chooser->setLabel($this->escapeHtml($data->getTitle()));
            }
        }

        $element->setData('after_element_html', $chooser->toHtml());
        return $element;
    }

    /**
     * Grid Row JS Callback
     *
     * @return string
     */
    public function getRowClickCallback()
    {
        $chooserJsObject = $this->getId();
        $js = '
            function (grid, event) {
                var trElement = Event.findElement(event, "tr");
                var blockId = trElement.down("td").innerHTML.replace(/^\s+|\s+$/g,"");
                var blockTitle = trElement.down("td").next().innerHTML;
                ' .
            $chooserJsObject .
            '.setElementValue(blockId);
                ' .
            $chooserJsObject .
            '.setElementLabel(blockTitle);
                ' .
            $chooserJsObject .
            '.close();
            }
        ';
        return $js;
    }

    /**
     * Prepare Cms static blocks collection
     *
     * @return \Magento\Backend\Block\Widget\Grid\Extended
     */
    protected function _prepareCollection()
    {
        $this->_datastoreRepository->getByWpId(10);

        $this->setCollection($this->_datastoreRepository->getListData());
        //var_dump($col);
        return parent::_prepareCollection();

    }

    /**
     * Prepare columns for Cms blocks grid
     *
     * @return \Magento\Backend\Block\Widget\Grid\Extended
     */
    protected function _prepareColumns()
    {

       //$this->_datastoreRepository->getById(100);
       /* var_dump($list[1]->getData());*/
       /*foreach ($list as $l)
       {
           var_dump($l);
       }*/
        //var_dump($this->_datastoreRepository->getMaxDate());

        $this->addColumn(
            'chooser_id',
            ['header' => __('EntitY_ID'), 'align' => 'right', 'index' => 'entity_id', 'width' => 50, 'index' => 'entity_id']
        );

        $this->addColumn('chooser_title', ['header' => __('Title'), 'align' => 'left', 'index' => 'title']);

        $this->addColumn(
            'chooser_identifier',
            ['header' => __('Content'), 'align' => 'left', 'index' => 'content']
        );
        $this->addColumn(
            'chooser_Item_id',
            ['header' => __('Item_ID'), 'align' => 'left', 'index' => 'item_id']
        );

        $this->addColumn(
            'chooser_css',
            ['header' => __('CSS'), 'align' => 'left', 'index' => 'css']
        );

        return parent::_prepareColumns();
    }

    /**
     * Get grid url
     *
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('datastore/block_widget/chooser', ['_current' => true]);
    }
}

