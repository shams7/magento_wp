<?php


namespace Its\DataStore\Setup;
use Magento\Cms\Model\Page;
use Magento\Cms\Model\PageFactory;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;

class UpgradeData implements UpgradeDataInterface
{
    /**
     * @var \Magento\Cms\Model\BlockFactory
     */
    protected $blockFactory;

    /**
     * @var \Magento\Cms\Model\BlockRepository
     */
    protected $blockRepository;
    /**
     * @param PageFactory $resultPageFactory
     * @param \Magento\Cms\Model\BlockFactory $blockFactory
     * @param \Magento\Cms\Model\BlockRepository $blockRepository
     */
    public function __construct(
        PageFactory $resultPageFactory,
        \Magento\Cms\Model\BlockFactory $blockFactory,
        \Magento\Cms\Model\BlockRepository $blockRepository
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->blockFactory = $blockFactory;
        $this->blockRepository = $blockRepository;
    }
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '0.0.1') < 0) {
            $testBlock = [
                'title' => 'Test block title',
                'identifier' => 'test-block',
                'stores' => [0],
                'is_active' => 1,
            ];
            $this->blockFactory->create()->setData($testBlock)->save();
        }

        $setup->endSetup();
    }

}
