<?php


namespace Its\DataStore\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;

use Magento\Framework\Setup\ModuleContextInterface;

use Magento\Framework\Setup\SchemaSetupInterface;


class UpgradeSchema implements   UpgradeSchemaInterface
{
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();
        if ($installer->getConnection()->tableColumnExists('cms_content', 'content')){
            $definition = [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'nullable' => false,
                'comment' => 'CMS Page Content'
            ];
            $installer->getConnection()->modifyColumn(
                $setup->getTable('cms_content'),
                'content',
                $definition
            );
        }
    }
}


