<?php


namespace Its\DataStore\Setup\Patch\Data;


use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\PatchInterface;

class AddDataFromCms implements \Magento\Framework\Setup\Patch\DataPatchInterface
{  /**
 * @var ModuleDataSetupInterface
 */
    private $moduleDataSetup;


    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup

    ) {
        $this->moduleDataSetup = $moduleDataSetup;

    }


    /**
     * @inheritDoc
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function apply()
    {
        $this->moduleDataSetup->startSetup();
        $setup = $this->moduleDataSetup;
        $data[] =
            [
            'title' => 'Sample Title 4',
            'content' => 'Sample content',
            'item_id' => 2,
            'css' => 'Sample css'

             ];
        $data[] =
            [
                'title' => 'Sample Title 3',
                'content' => 'Sample content',
                'item_id' => 4,
                'css' => 'Sample css'

            ];


        $this->moduleDataSetup->getConnection()->insertArray(
            $this->moduleDataSetup->getTable('cms_content'),
            ['title', 'content', 'item_id',   'css'],
            $data
        );
        $this->moduleDataSetup->endSetup();
    }
}
