<?php


namespace Its\DataStore\Api\Data;

/**
 * Interface DataStoreInterface
 *
 * @api
 */
interface DataStoreInterface
{
    /**
     * @return int
     */
    public function getId();

    /**
     * @param int $entity_id
     * @return $this
     */
    public function setId($entity_id);

    /**
     * @return string
     */
    public function getTitle();

    /**
     * @param string $title
     * @return $this
     */
    public function setTitle($title);

    /**
     * @return string
     */
    public function getContent();

    /**
     * @param string $content
     * @return $this
     */
    public function setContent($content);

    /**
     * @return int
     */
    public function getItemId();

    /**
     * @param int $item_id
     * @return $this
     */
    public function setItemId($item_id);
    /**
     * @return string
     */
    public function getCss();

    /**
     * @param string $css
     * @return $this
     */
    public function setCss($css);


}
