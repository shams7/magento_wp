<?php


namespace Its\DataStore\Api;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Its\DataStore\Api\Data\DataStoreInterface;
use Magento\Framework\Api\SearchCriteriaInterface;

/**
 * Interface DataStoreRepositoryInterface
 *
 * @api
 */
interface DataStoreRepositoryInterface
{
    /**
     * Create or update a DataStore.
     *
     * @param DataStoreInterface $datastore
     * @return DataStoreInterface
     */
    public function save(DataStoreInterface $datastore);

    /**
     * Get a DataStore by Id
     *
     * @param int $id
     * @return DataStoreInterface
     * @throws NoSuchEntityException If DataStore with the specified ID does not exist.
     * @throws LocalizedException
     */
    public function getById($id);
    /**
     * Get a DataStore by Id
     *
     * @param int $itemid
     * @return DataStoreInterface
     * @throws NoSuchEntityException If DataStore with the specified ID does not exist.
     * @throws LocalizedException
     */
    public function getByItemId($itemid);
    /**
     * Retrieve DataStore which match a specified criteria.
     *
     * @param SearchCriteriaInterface $criteria
     */
    public function getList(SearchCriteriaInterface $criteria);
    /**
     * Delete a DataStore
     *
     * @param DataStoreInterface $datastore
     * @return DataStoreInterface
     * @throws NoSuchEntityException If DataStore with the specified ID does not exist.
     * @throws LocalizedException
     */
    public function delete(DataStoreInterface $datastore);
    /**
     * Delete a DataStore by Id
     *
     * @param int $id
     * @return DataStoreInterface
     * @throws NoSuchEntityException If DataStore with the specified ID does not exist.
     * @throws LocalizedException
     */
    public function deleteById($id);
    /**
     * Retrieve DataStore which match a specified criteria.
     *
     */
    public function getListData();

    public function insertBlockCms();
    public function getListDataStore();
    public function getByWpId($itemid);
    public function getMaxDate();
}
